﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<MyPin> pins;
        public MainPage()
        {
            BindingContext = this;
            InitializeComponent();
            pins = new ObservableCollection<MyPin>();
            MyPin moscow = new MyPin
            {
                Label = "Moscow",
                Address = "Capital of Russia",
                Type = PinType.Place,
                Position = new Position(55.7527698, 37.6213976)
            };
            MyPin iguazu = new MyPin
            {
                Label = "Iguazu Falls",
                Address = "Best waterfalls in the world",
                Type = PinType.Place,
                Position = new Position(-25.6911192, -54.4498087)
            };
            MyPin empstate = new MyPin
            {
                Label = "Empire State Building",
                Address = "I've been there once",
                Type = PinType.Place,
                Position = new Position(40.7484445, -73.9878531)
            };
            MyPin disney = new MyPin
            {
                Label = "Disney Parks",
                Address = "My childhood is there",
                Type = PinType.Place,
                Position = new Position(28.3740649, -81.572958)
            };
            UserMap.Pins.Add(moscow);
            pins.Add(moscow);
            UserMap.Pins.Add(iguazu);
            pins.Add(iguazu);
            UserMap.Pins.Add(empstate);
            pins.Add(empstate);
            UserMap.Pins.Add(disney);
            pins.Add(disney);

            UserPicker.ItemsSource = pins;
        }

        public void OnPickerIndexChanged(object sender, EventArgs e)
        {
            MyPin pin = pins[((Picker)sender).SelectedIndex];
            Debug.WriteLine(pin.Position);
            UserMap.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromKilometers(5)));
        }

        public void OnButtonPressed(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if(UserMap.MapType == MapType.Street)
            {
                UserMap.MapType = MapType.Satellite;
                button.Text = "Change to Street";
            }
            else
            {
                UserMap.MapType = MapType.Street;
                button.Text = "Change to Satellite";
            }
        }
    }

    public class MyPin : Pin
    {
        public override string ToString()
        {
            return Label;
        }
    }
}
